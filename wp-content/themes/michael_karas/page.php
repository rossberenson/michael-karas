<?php get_header(); ?>
	
	<div id="content">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>   
			 
				<div class="row">
					<div class="twelve columns">
						<h1><?php the_title(); ?></h1>
						<div class="entrytext"><?php the_content(); ?></div>
					</div>
				</div>
				
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	
<?php get_footer(); ?>