/*===========================================================================================================*/
/*																					Document Ready			*/
/*=========================================================================================================*/

$(document).ready(function(){

/*==============================================================================*/
/*													  SVG to PNG	  		   */
/*============================================================================*/


    if(!Modernizr.svg) {
	    $('img[src*="svg"]').attr('src', function() {
	        return $(this).attr('src').replace('.svg', '.png');
	    });
	}

	


/*==============================================================================*/
/*													  Page load hero size      */
/*============================================================================*/

function heroHeight(){
    // Get the dimensions of the viewport
    var windowheight = $(window).height();
    var changeHeight = windowheight - $('#videos').height() - 130 + 'px';
   
   if (windowheight > 924) {
	   $('.fill, #slider .slide, #hero .bx-wrapper, #hero .bx-viewport').css({height :changeHeight});
   }
};

$(document).ready(heroHeight);    // When the page first loads
$(window).resize(heroHeight);     // When the browser changes size


/*==============================================================================*/
/*													  Slider			       */
/*============================================================================*/

$('#slider').bxSlider({
	mode: 'fade',
	speed: 800,
	pause: 8000,
	auto: true,
	pager: false,
	controls: false
});

$('#videos ul').bxSlider({
	speed: 800,
	auto: false,
	slideMargin: 18,
	maxSlides: 3,
	slideWidth: 248,
	pager: false,
	moveSlides: 1,
	nextSelector: '#videos .next',
	prevSelector: '#videos .prev'
});

/*==============================================================================*/
/*													  Smooth Scroll		       */
/*============================================================================*/


$('#content').pageScroller({ 
	sectionClass: 'section',
	navigation: '.header_menu',
	HTML5mode: false,
	scrollOffset: -50
});

/*==============================================================================*/
/*													  Pretty Photo		       */
/*============================================================================*/


 $("a[rel^='prettyPhoto']").prettyPhoto();
 

/*==============================================================================*/
/*													  Clients			       */
/*============================================================================*/ 
 

$('.client:last-child').addClass('end');

function equalHeight(group) {
		var tallest = 0;
		group.each(function() {
			var thisHeight = $(this).outerHeight();
			if(thisHeight > tallest) {
				tallest = thisHeight;
			}
		});
		group.height(tallest);
	}
	
	equalHeight( $('.client'));

                
$('.slide').each(function(){
	var slide = $(this); // assigning the object
	    window = $(window);
    
	$(window).scroll(function() {
    
		// Scroll the background at var speed
		// the yPos is a negative value because we're scrolling it UP!								
		var yPos = -$(window).scrollTop() / slide.data('speed'); 
		
		// Put together our final background position
		var coords = '50% '+ yPos + 'px';
		
		// Move the background
		slide.css({ backgroundPosition: coords });
		slide.addClass('YEA');

	}); // window scroll Ends
});




/*==============================================================================*/
/*													  Award wrapper			       */
/*============================================================================*/  
var $children = $('.award.six');
for(var i = 0, l = $children.length; i < l; i += 2) {
    $children.slice(i, i+2).wrapAll('<div class="row"></div>');
}



}); // Close Document Ready