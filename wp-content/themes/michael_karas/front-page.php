<?php get_header(); ?>
	
	<div id="hero" class="section"> 
		<div id="slider">
			<?php $loop = new WP_Query( array( 'post_type' => 'slide', 'posts_per_page' => 5 ) );
					while ( $loop->have_posts() ) : $loop->the_post(); ?>
						<div class="slide" style=" background-image: url(<?php echo get('slide_image'); ?>);">
							<div class="row quote_wrap">
								<div class="twelve columns">
									<div class="row container">
										<?php 
											echo '<div class="quote">"' . get('quote') . '"</div>';
											echo '<div class="author">- ' . get('author') . '</div>';
										?>
									</div>
								</div>
							</div>
						</div>
						
				<?php wp_reset_query();
			endwhile; ?>	
		</div>
	</div>
	<div class="fill"></div>
	
	<div id="content">
		<div id="booking" class="row hide-for-small">
			<div class="twelve columns">
				<div class="row container">
					<div class="content">
						<div class="booking three columns">
							<p>Interested in booking me?<br/>See if I'm available!</p>
						</div>
						<div class="nine columns">
							 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Booking') ) : ?><?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<section id="videos" class="section row dark">
			<div class="twelve columns">
				<div class="row container title">
					<div class="twelve columns">
						<h4>Videos</h4>
					</div>
				</div>
				<div class="row container">
					<div class="one column mobile-one prev slide_btn"></div>
					<div class="ten mobile-two columns">
						<ul class="video_slider">	
							<?php 
								$args = array( 'post_type' => 'video', 'posts_per_page' => -1 );
								$loop = new WP_Query( $args ); 
								while ( $loop->have_posts() ) : $loop->the_post(); ?>
									<li>
										<a rel="prettyPhoto" href="<?php echo get('video_link'); ?>">
										<?php echo get_image('image'); ?>
										<span class="watch"></span>
										</a>
									</li>
									<?php wp_reset_query();
								endwhile;
							?>
						</ul>
					</div>
					<div class="one column mobile-one next slide_btn"></div>
				</div>
			</div>
		</section>
		
		
		<section id="about" class="section">
			<div class="row container">
			
				<div class="seven columns">
					<?php 
						$args = array( 'post_type' => 'about', 'posts_per_page' => 1 );
						$loop = new WP_Query( $args ); 
						while ( $loop->have_posts() ) : $loop->the_post();
	
							$headlines = get_field('headline_line');
	
							foreach($headlines as $headline){
								echo "<h2>".$headline."</h2>";
							}
							?>
	
							<div class="text"><?php the_content(); ?></div>
						<?php endwhile;
					?>
				</div>
			</div>
		</section>
		
		<div class="break"></div>
		
		<section id="clients" class="section dark">
			<div class="row container">
				<div class="seven columns clients">
					<h4>Clients</h4>
					<ul class="row">
					<?php 
						$args = array( 'post_type' => 'client', 'posts_per_page' => 100 );
						$loop = new WP_Query( $args ); 
						while ( $loop->have_posts() ) : $loop->the_post(); ?>
						
						<li class="client three columns">
							<?php the_post_thumbnail('client'); ?>
						</li>
						
					<?php endwhile;	?>
					</ul>
				</div>
				<div id="awards" class="four columns">
					<div class="row">
							<h4>Awards</h4>				
					
							<?php 
								$args = array( 'post_type' => 'award', 'posts_per_page' => 100 );
								$loop = new WP_Query( $args ); 
								while ( $loop->have_posts() ) : $loop->the_post(); ?>
								
								
								<div class="six columns">
									<div class="award">
										<div class="award-content">
											<div class="line_one"><?php echo get('line_one'); ?></div>
											<div class="line_two"><?php echo get('line_two'); ?></div>
										</div>
									</div>
								</div>
							<?php endwhile;	?>
					</div>
				</div>
			</div>						
			
		</section>
		
		<div class="break"></div>
		
		<section id="contact" class="section">
			<div class="row container">
				<div class="three columns">
					<img src="<?php bloginfo('template_directory'); ?>/images/contact.jpg" alt="contact" width="281" height="348" />
				</div>
				<div class="four columns">
					<?php echo do_shortcode( '[contact-form-7 id="19" title="Contact Form"]' ); ?>
				</div>
				<div class="five columns">
					<div class="entry-text">
											 <?php
							// must use a variable for page id
							// http://codex.wordpress.org/Function_Reference/get_page
							$id = 25;
							$p = get_page($id);
							echo apply_filters('the_content', $p->post_content);
						?>
					</div>
				</div>
			</div>
		</section>
		
	</div><!-- end content -->
	
<?php get_footer(); ?>