<?php

/*===========================================================================================================*/
/*																Theme Stylesheets using Enqueue				*/
/*=========================================================================================================*/
function michael_karas_styles()
{

    wp_register_style('foundation', get_template_directory_uri() . '/css/foundation.css', array(), 'all');
    wp_register_style('font_awesome-ie', get_template_directory_uri() . '/css/font-awesome-ie.css', array(), 'all');
    wp_register_style('main_style', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    
    wp_enqueue_style('foundation'); // Enqueue it!
    wp_enqueue_style('font_awesome'); // Enqueue it!
    wp_enqueue_style('main_style'); // Enqueue it!
}

add_action('wp_enqueue_scripts', 'michael_karas_styles'); // Add Theme Stylesheet

add_editor_style();

/*===========================================================================================================*/
/*																Theme Javascript using Enqueue				*/
/*=========================================================================================================*/

function michael_karas_scripts() {
    wp_enqueue_script( 'modernizer', get_template_directory_uri() . '/js/vendor/modernizr-2.6.2.min.js', array(), '2.6.2', false );
    
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js', array(), '', false );
    wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins.js', array('jquery'), '', true );
    wp_enqueue_script( 'script', get_template_directory_uri() . '/js/main.js', array('jquery'), '', true );

    wp_enqueue_script( 'modernizer' ); // Enqueue it!
    wp_enqueue_script( 'jquery' ); // Enqueue it!
    wp_enqueue_script( 'plugins' ); // Enqueue it!
    wp_enqueue_script( 'script' ); // Enqueue it!
}    
 
add_action('wp_enqueue_scripts', 'michael_karas_scripts');


// ===== add ie conditional html5 shim to header
function add_ie_html5_shim () {
    echo '<!--[if lt IE 9]>';
    echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
    echo '<![endif]-->';
}
add_action('wp_head', 'add_ie_html5_shim');


/*===========================================================================================================*/
/*																		Add Custom Thumbnails				*/
/*=========================================================================================================*/
	add_theme_support( 'post-thumbnails' ); 
	
	the_post_thumbnail();                  // without parameter -> Thumbnail	
	add_image_size( 'hero', 1800, 700 ); // Hero thumbnail size
	add_image_size( 'client', 100, 9999 ); // Blog thumbnail size
	add_image_size( 'video', 310, 140 ); // Blog thumbnail size

	
	
/*===========================================================================================================*/
/*																		Add Custom Menus					*/
/*=========================================================================================================*/

add_theme_support( 'menus' );

register_nav_menu( 'header', 'Header' );
//register_nav_menu( 'footer', 'Footer' );


/*===========================================================================================================*/
/*																		Add Widget Area						*/
/*=========================================================================================================*/

if ( function_exists('register_sidebars') ) {
	register_sidebar(array(
		'name'=>'Footer Booking',
		'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="title">',
        'after_title' => '</div>',
	));
	
	//register_sidebar(array('name'=>'Sidebar 2',));
	//register_sidebar(array('name'=>'Sidebar 3',));
}

    

/*===========================================================================================================*/
/*																		Misc								*/
/*=========================================================================================================*/

// =====  Remove Injected classes, ID's and Page ID's from Navigation <li> items
	function my_css_attributes_filter($var)
	{
    	return is_array($var) ? array() : '';
    }

	// =====  Remove invalid rel attribute values in the categorylist
	function remove_category_rel_from_category_list($thelist)
	{
	    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
	}

	// =====  Add page slug to body class, love this - Credit: Starkers Wordpress Theme
	function add_slug_to_body_class($classes)
	{
	    global $post;
	    if (is_home()) {
	        $key = array_search('blog', $classes);
	        if ($key > -1) {
	            unset($classes[$key]);
	        }
	    } elseif (is_page()) {
	        $classes[] = sanitize_html_class($post->post_name);
	    } elseif (is_singular()) {
	        $classes[] = sanitize_html_class($post->post_name);
	    }
	
	    return $classes;
	}


    function theme_body_class($classes){
        $theme = wp_get_theme();
        $classes[] = sanitize_title_with_dashes($theme->Name);
        return $classes;
    }

    add_filter('body_class', 'theme_body_class');
    
 function my_custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url('.get_bloginfo('template_directory').'/images/karas_logo.png) !important; background-size: 300px 40px!important; }
    </style>';
}

add_action('login_head', 'my_custom_login_logo');
	

/*===========================================================================================================*/
/*																		Shortcode							*/
/*=========================================================================================================*/   
    
 // Include shortcodes
include 'shortcodes.php';