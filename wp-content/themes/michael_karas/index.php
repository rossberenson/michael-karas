<?php get_header(); ?>
	
	<div id="slider">
		<?php 
			$args = array(
		        'post_type' => 'slide',
		        'post_status' => 'publish',
		        'posts_per_page' => -1
		    );
		     
		    $posts = new WP_Query( $args );
		    if ( $posts -> have_posts() ) {
		        while ( $posts -> have_posts() ) {
		        	the_content();
		        }
		    }
		                
		    wp_reset_query();
    	?>
	</div>
	
	<div id="content">
	</div>
	
<?php get_footer(); ?>