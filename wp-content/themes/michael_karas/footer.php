<footer>
	<div class="bar clearfix">
		<div class="row container">
			<div class="twelve columns">
				<div class="copyright">© <?php echo date("Y"); ?> Michael Karas</div>
				<a class="contact" href="mailto:booking@michaelkarasonline.com">booking@michaelkarasonline.com</a>
			</div>
		</div>
	</div>
	<div class="break"></div>
</footer>

<?php wp_footer(); ?>
</body>
</html>