<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	 <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
	 <meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	<link href='http://fonts.googleapis.com/css?family=Roboto:700,400,100,300,400italic,100italic' rel='stylesheet' type='text/css'>
</head>

<?php wp_head(); ?>

<body <?php body_class(); ?>>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

	<header class="row hide-for-small">
		<div class="twelve columns">
			<div class="row container">
				<div id="logo" class="two columns"><a><img src="<?php bloginfo('template_directory'); ?>/images/karas_logo.svg" alt="logo"/><span class="visuallyhidden">Michael Karas</span></a></div>
				<?php wp_nav_menu( array( 'theme_location' => 'header', 'container' => '', 'menu_class' => 'header_menu eight columns'  )); ?>
				<div class="social one columns">
					<a target="_blank" class="twitter first" aria-hidden="true" data-icon="&#xe000;" href="https://twitter.com/nycjuggler"></a>
					<a target="_blank" class="facebook" aria-hidden="true" data-icon="&#xe001;" href="https://www.facebook.com/karasjuggling"></a>
					<a target="_blank" class="youtube last" aria-hidden="true" data-icon="&#xe002;" href="http://www.youtube.com/user/MichaelAKaras"></a>
				</div>
			</div>
		</div>
	</header>
	<header class="mobile-menu row show-for-small">
		<div class="twelve columns">
			<div class="row container">
				<div id="logo" class="mobile-three columns"><a><img src="<?php bloginfo('template_directory'); ?>/images/karas_logo.svg" alt="logo"/><span class="visuallyhidden">Michael Karas</span></a></div>
				<div class="social mobile-one columns end">
					<a target="_blank" class="twitter first" aria-hidden="true" data-icon="&#xe000;" href="https://twitter.com/nycjuggler"></a>
					<a target="_blank" class="facebook" aria-hidden="true" data-icon="&#xe001;" href="https://www.facebook.com/karasjuggling"></a>
					<a target="_blank" class="youtube last" aria-hidden="true" data-icon="&#xe002;" href="http://www.youtube.com/user/MichaelAKaras"></a>

				</div>
				<div class="row">
					<?php wp_nav_menu( array( 'theme_location' => 'header', 'container' => '', 'menu_class' => 'header_menu mobile-four columns'  )); ?>
				</div>
			</div>
		</div>
	</header>