<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'karas2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$@1oE+~@W(pM/INsD;9JEw,RW/^CP`^n8cNEAi }Pg QZ@Mv{{>KbAN_)_=R;_AR');
define('SECURE_AUTH_KEY',  '-|0Y1:tw9--N6[{e;7Sr6CeVuV& S,dTk$b=MI+Vs[$up+ZWdbDn?1dzlbe6dt5<');
define('LOGGED_IN_KEY',    '-NL#scM%YGBMf7WC*|_%icfpUf@tOb|Fq/Dq:k;uGDL>U|Q-al=<uih$wl:<+P[.');
define('NONCE_KEY',        '+p G.G5h+*CB9Pa{4c~#=.Y,l3:SxO_INe^t gCVob$|f%LX^ u:vkhRmh%4gE:r');
define('AUTH_SALT',        '{&K`eyjm?ad|KRpT6%99t#5sr0}Zjv|Q`dos~G^C8{ubQAzQn;Bc{]7wFR!p7b3;');
define('SECURE_AUTH_SALT', 'BzdF20I?z/*(p/t;-BaD2eC(N~;HyU8c!JS_:4}qF1@J;6odZNsocDIM|<)CV<2m');
define('LOGGED_IN_SALT',   '3/(hR%K1Hav)wm1<Y[5CZLUo%M8BQ|&Z}6n^RV$>_>y1~9,^S{g0VvH-b#(`hUo]');
define('NONCE_SALT',       'k/-xxW%oGyRJWT@=(3>,z:,PN+,*`r?f3cksb-;/*X2Mr];eK!+ok$}m#c9!/#cj');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
